FROM openjdk:8u111-jdk-alpine
VOLUME /tmp
EXPOSE 8080:8080
ADD /target/*.jar app.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]
ENV TOPIC spring-boot-exchange
ENV QUEUE spring-boot
ENV ROUTE foo.bar.env
