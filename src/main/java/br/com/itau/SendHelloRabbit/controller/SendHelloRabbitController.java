package br.com.itau.SendHelloRabbit.controller;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@CrossOrigin
public class SendHelloRabbitController {

	@Value("${topic.ExchangeName}")
	private String topicExchangeName;
	// = "spring-boot-exchange";
	@Value("${queue.Name}")
	private String queueName ;
	//= "spring-boot";
	@Value("${route.Name}")
	private String routeName;
	//= "foo.bar.";
	
	@Autowired
	private final RabbitTemplate rabbitTemplate = new RabbitTemplate();

	@RequestMapping("/send")
	public @ResponseBody String sendMsg(@RequestParam("msg") String msg) {
		rabbitTemplate.convertAndSend(topicExchangeName, routeName + "baz", msg);
		return "OK";
	}
    @RequestMapping(path = "/send", method = RequestMethod.POST)
	public @ResponseBody String sendMsga(@RequestBody String msgPost) {
		rabbitTemplate.convertAndSend(topicExchangeName, routeName + "baz", msgPost);
		return "OK";
	}
}

